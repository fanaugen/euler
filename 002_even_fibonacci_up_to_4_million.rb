# By considering the terms in the Fibonacci sequence whose values
# do not exceed four million, find the sum of the even-valued terms.

# this Enumerator produces an infinite sequence
fibonacci = Enumerator.new do |yielder|
  a = b = 1
  loop do
    yielder.yield a
    a, b = b, a + b
  end
end

puts fibonacci.take_while{|n| n <= 4_000_000}.select(&:even?).reduce(&:+)
