# What is the largest prime factor of the number 600851475143 ?

# The easiest way, of course, is to use the Prime class from
# Ruby’s standard library:

require 'prime'

n = 600851475143
generator = Prime::EratosthenesGenerator.new
factorization = Prime.instance.prime_division(n, generator)
# returns an array of pairs, where the first member is the
# prime and the second is the exponent

largest_prime_factor = factorization.reduce(2) do |memo, pair|
  memo = pair[0] if pair[0] > memo
end

puts "The largest prime factor of #{n} is #{largest_prime_factor}"

# TODO: post this on the projecteuler forum!
